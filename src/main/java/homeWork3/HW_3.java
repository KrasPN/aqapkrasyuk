package homeWork3;

import java.util.Scanner;

public class HW_3 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите имя: ");
        String name = scan.nextLine();
        System.out.println("Введите возраст: ");
        int age = scan.nextInt();
        System.out.println("Введите сумму кредита: ");
        double sum = scan.nextDouble();
        double creditResult = age * 100;

        String credit = (!name.equalsIgnoreCase("Bob")) && (age >= 18) && !(sum > creditResult) ? "Кредит выдан" : "Кредит не выдан";
        System.out.println(credit);
    }
}
